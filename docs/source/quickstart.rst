##########
Quickstart
##########

Installation and Setup
^^^^^^^^^^^^^^^^^^^^^^^

Pre-requisites:
---------------

    Before proceding further please ensure *pip* and *python* is installed and configured.

    In case you have issues installing python-3.7, we recommend using `pyenv`.
    
    If you are working on Ubuntu/Debian systems make sure you have the following dependencies
    installed before installing `pyenv`

    .. code-block:: bash

      sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
      libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
      xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
      
    Download and install pyenv:

    .. code-block:: bash

      #!/bin/sh
      curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

    Add the following lines to your bashrc:

    .. code-block:: bash

      export PATH="/home/<username>/.pyenv/bin:$PATH"
      eval "$(pyenv init -)"
      eval "$(pyenv virtualenv-init -)"

    Open a new terminal and create a virtual environment using the following

    .. code-block:: bash

      pyenv install 3.7.0
      pyenv virtualenv 3.7.0 riscof_env


    Now you can activate this virtual environment using the following command:

    .. code-block:: bash

      pyenv activate riscof_env
      python --version

Installing RISCOF:
------------------
      
  * Install using pip (For users):

    .. code-block:: bash

        pip install riscof

    To upgrade an already installed version of riscof to the latest version:

    .. code-block:: bash

        pip install -U riscof

    To pull out a specific version of riscof:

    .. code-block:: bash

        pip install riscof==1.X.X



  * Clone from git(For developers):

    .. code-block:: bash

        git clone https://gitlab.com/incoresemi/riscof.git
        cd riscof
        pip install -r requirements.txt

Download sample plugins:
------------------------

    * Clone the plugins from git.

    .. code-block:: bash

        git clone https://gitlab.com/incoresemi/riscof-plugins.git

    * Follow the steps given in the respective plugin folders to set them up.
    * Add the paths of the plugins you wish to use to the *PYTHONPATH* variable in your *.bashrc* or by using the *export* command.

Usage
^^^^^

* For users 

.. code-block:: bash

    usage: riscof [-h] [--setup] [--validateyaml] [--testlist] [--run] [--verbose]
    
    This program checks compliance for a DUT.
    
    optional arguments:
      --run           Run riscof in current directory.
      --setup         Initiate setup for riscof.
      --testlist      Generate the testlist only.
      --validateyaml  Validate the Input YAMLs using riscv-config
      --verbose       debug | info | warning | error
      -h, --help      show this help message and exit

* For developers

.. code-block:: bash

    cd riscof/

    python3 -m riscof.main -h

      usage: riscof [-h] [--setup] [--validateyaml] [--testlist] [--run] [--verbose]
      
      This program checks compliance for a DUT.
      
      optional arguments:
        --run           Run riscof in current directory.
        --setup         Initiate setup for riscof.
        --testlist      Generate the testlist only.
        --validateyaml  Validate the Input YAMLs using riscv-config
        --verbose       debug | info | warning | error
        -h, --help      show this help message and exit


Example
^^^^^^^

This Example runs spike vs sigGen. Please ensure spike and riscv toolchain is installed and configured before running this.

1. Setup

    * For users

    .. code-block:: bash

        riscof --setup

    * For developers

    .. code-block:: bash

        python3 -m riscof.main --setup

    A *config.ini* file and *env* directory will be created in the *pwd*.

2. Configure
    
    Modify the config.ini file as follows. The *env* directory can be ignored for now.

    .. code-block:: ini

        [RISCOF]
        ReferencePlugin=sigGen
        DUTPlugin=spike

        [spike]
        ispec=#/path_to_riscof_plugins/yamlPlugin/Examples/rv32i_isa.yaml
        pspec=#/path_to_riscof_plugins/yamlPlugin/Examples/rv32i_platform.yaml
    
    In the above block please edit the paths to point to the files appropriately. Other plugins can be used in the same way by changing the names in the nodes and the DUTPlugin argument.
    Remember to update your PYTHONPATH to include the plugins:

    .. code-block:: bash

        export PYTHONPATH=<path_to_riscof-plugins>/signGen:<path_to_riscof-plugins>/spike_simple

3. Run

    * For users

    .. code-block:: bash

        riscof --run --verbose debug

    * For developers
    
    .. code-block:: bash

        python3 -m riscof.main --run --verbose debug



Writing your own Plugins
^^^^^^^^^^^^^^^^^^^^^^^^^
* Ensure that the module is named as *riscof_\*model_name\*.py* and the class is named as *model_name*.
* The class is a subclass of the *pluginTemplate* class present in *riscof.pluginTemplate*.
* The path where the file exists is present on the *PYTHONPATH*.

.. code-block:: python

    #riscof_sample.py
    from riscof.pluginTemplate import pluginTemplate
    class sample(pluginTemplate):
        def __init__(self,*args,**kwargs):
            super().__init__(*args,**kwargs)
            #Your code here
        
        def initialise(self,suite,workdir):
            super().initialise(suite,workdir)
            #Your code here
        
        def build(self,isa_spec,platform_spec):
            super().build(isa_spec,platform_spec)
            #Your code here
        
        def runTests(self, testlist):
            super().build(testlist)
            #Your code here.

